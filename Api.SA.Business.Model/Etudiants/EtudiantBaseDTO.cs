﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SA.Business.Model.Etudiants
{
    public class EtudiantBaseDTO
    {
        public string EtudiantNom { get; set; }
        public string EtudiantPrenom { get; set; } = null!;
        public string EtudiantAdresse { get; set; }
        public string EtudiantEmail { get; set; }
        public string EtudiantTelephone { get; set; }
        public DateTime EtudiantDateAnniversaire { get; set; }
    }
}
