﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SA.Business.Model.Etudiants
{
    public class EtudiantReadDTO : EtudiantBaseDTO
    {
        public int EtudiantId { get; set; }
    }
}
