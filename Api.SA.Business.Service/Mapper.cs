﻿using Api.SA.Business.Model.Etudiants;
using Api.simplon_academy.Datas.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SA.Business.Service
{
    public static class Mapper
    {
        public static EtudiantReadDTO TransformEtudiantToDTO(Etudiant etudiant)
        {
            EtudiantReadDTO etudiantRead = new EtudiantReadDTO()
            {
                EtudiantId = etudiant.Id,
                EtudiantNom = etudiant.Nom,
                EtudiantPrenom = etudiant.Prenom,
                EtudiantAdresse = etudiant.Adresse,
                EtudiantEmail = etudiant.Email,
                EtudiantDateAnniversaire = (DateTime)etudiant.DateAnniversaire,
                EtudiantTelephone = etudiant.Telephone
            };

            return etudiantRead;
        }

        //public static EtudiantAddDTO TransformDTOToEtudiant(Etudiant etudiant)
        //{
        //    EtudiantAddDTO etudiantAdd = new EtudiantAddDTO()
        //    {
        //        etudiant.Nom = EtudiantNom,
        //        etudiant.Prenom = Etudiandn<ntPrenom,
        //        etudiant.Adresse = EtudiantAdresse,
        //        etudiant.Email = EtudiantEmail,
        //        EtudiantDateAnniversaire = (DateTime)etudiant.DateAnniversaire,
        //        EtudiantTelephone = etudiant.Telephone
        //    };

        //    return etudiantAdd;
        //}
    }
}
