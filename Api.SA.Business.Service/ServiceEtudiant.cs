﻿using Api.SA.Business.Model.Etudiants;
using Api.SA.Business.Srvc.Ctr;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SA.Business.Service
{
    public class ServiceEtudiant : IServiceEtudiant
    {
        private readonly IRepositoryEtudiant _repositoryEtudiant;

        public ServiceEtudiant(IRepositoryEtudiant repositoryEtudiant)
        {
            _repositoryEtudiant = repositoryEtudiant;
        }

        public async Task<EtudiantReadDTO> CreateEtudiantAsync(EtudiantAddDTO etudiantToRead)
        {
            Etudiant newEtudiant = new Etudiant()
            {
                Nom = etudiantToRead.EtudiantNom,
                Prenom = etudiantToRead.EtudiantPrenom,
                Adresse = etudiantToRead.EtudiantAdresse,
                Email = etudiantToRead.EtudiantEmail,
                Telephone = etudiantToRead.EtudiantTelephone,
                DateAnniversaire = etudiantToRead.EtudiantDateAnniversaire
            };

            var etudiant = await _repositoryEtudiant.CreateElementAsync(newEtudiant).ConfigureAwait(false);

            //utiliser le readDTO
            return Mapper.TransformEtudiantToDTO(etudiant);
        }

        //public async Task<List<EtudiantReadDTO>> GetListEtudiantAsync()
        //{
        //    var etudiants = await _repositoryEtudiant.GetAllAsync().ConfigureAwait(false);

        //    List<EtudiantReadDTO> etudiantDtos = new();

        //    foreach(var etudiant in etudiants)
        //    {
        //        etudiantDtos.Add(Mapper.TransformEtudiantToDTO(etudiant));
        //    }

        //    return etudiantDtos;
        //}

        Task<EtudiantAddDTO> IServiceEtudiant.CreateEtudiantAsync(EtudiantAddDTO etudiantToAdd)
        {
            throw new NotImplementedException();
        }

        public async Task<List<EtudiantReadDTO>> GetListEtudiantAsync()
        {
            var etudiants = await _repositoryEtudiant.GetAllAsync().ConfigureAwait(false);

            List<EtudiantReadDTO> etudiantDtos = new();

            foreach (var etudiant in etudiants)
            {
                etudiantDtos.Add(Mapper.TransformEtudiantToDTO(etudiant));
            }

            return etudiantDtos;
        }

        public async Task<EtudiantReadDTO> GetEtudiantByMailAsync(string mailEtudiant)
        {
            var etudiant = await _repositoryEtudiant.GetEtudiantByEmailAsync(mailEtudiant).ConfigureAwait(false);
            return Mapper.TransformEtudiantToDTO(etudiant);
        }
    }
}
