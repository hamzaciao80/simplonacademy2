﻿using Api.SA.Business.Model.Etudiants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.SA.Business.Srvc.Ctr
{
    public interface IServiceEtudiant
    {
        Task<List<EtudiantReadDTO>> GetListEtudiantAsync();

        Task<EtudiantReadDTO> GetEtudiantByMailAsync(string mailEtudiant);

        Task<EtudiantAddDTO> CreateEtudiantAsync(EtudiantAddDTO etudiantToAdd);
    }
}
