﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Categorie
    {
        public Categorie()
        {
            Matieres = new HashSet<Matiere>();
        }

        public int Id { get; set; }
        public string? Label { get; set; }

        public virtual ICollection<Matiere> Matieres { get; set; }
    }
}
