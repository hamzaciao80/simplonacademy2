﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Cour
    {
        public Cour()
        {
            CoursEtudiants = new HashSet<CoursEtudiant>();
            CoursProfesseurs = new HashSet<CoursProfesseur>();
            Examen = new HashSet<Examen>();
            Presences = new HashSet<Presence>();
            Salles = new HashSet<Salle>();
        }

        public int Id { get; set; }
        public int IdMatiere { get; set; }
        public int IdCycle { get; set; }
        public DateOnly? DateDebut { get; set; }
        public DateOnly? DateFin { get; set; }
        public bool? EstOuvert { get; set; }

        public virtual Cycle IdCycleNavigation { get; set; } = null!;
        public virtual Matiere IdMatiereNavigation { get; set; } = null!;
        public virtual ICollection<CoursEtudiant> CoursEtudiants { get; set; }
        public virtual ICollection<CoursProfesseur> CoursProfesseurs { get; set; }
        public virtual ICollection<Examen> Examen { get; set; }
        public virtual ICollection<Presence> Presences { get; set; }
        public virtual ICollection<Salle> Salles { get; set; }
    }
}
