﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class CoursEtudiant
    {
        public int Id { get; set; }
        public int IdCours { get; set; }
        public int IdEtudiant { get; set; }
        public DateOnly? DateDebut { get; set; }
        public DateOnly? DateFin { get; set; }

        public virtual Cour IdCoursNavigation { get; set; } = null!;
    }
}
