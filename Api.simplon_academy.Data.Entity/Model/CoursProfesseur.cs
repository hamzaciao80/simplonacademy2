﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class CoursProfesseur
    {
        public int Id { get; set; }
        public int? IdCours { get; set; }
        public int? IdProfesseur { get; set; }

        public virtual Cour? IdCoursNavigation { get; set; }
        public virtual Professeur? IdProfesseurNavigation { get; set; }
    }
}
