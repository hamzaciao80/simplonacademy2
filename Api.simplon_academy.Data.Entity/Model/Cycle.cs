﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Cycle
    {
        public Cycle()
        {
            Cours = new HashSet<Cour>();
        }

        public int Id { get; set; }
        public string? Titre { get; set; }
        public string? Description { get; set; }
        public DateTime? DateDebut { get; set; }
        public DateTime? DateFin { get; set; }

        public virtual ICollection<Cour> Cours { get; set; }
    }
}
