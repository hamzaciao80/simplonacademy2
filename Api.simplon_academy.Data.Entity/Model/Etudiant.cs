﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Etudiant
    {
        public Etudiant()
        {
            EtudiantNiveaus = new HashSet<EtudiantNiveau>();
            Notes = new HashSet<Note>();
            Presences = new HashSet<Presence>();
        }

        public int Id { get; set; }
        public string? Nom { get; set; }
        public string Prenom { get; set; } = null!;
        public string? Adresse { get; set; }
        public string? Email { get; set; }
        public string? Telephone { get; set; }
        public DateTime? DateAnniversaire { get; set; }
        public DateTime? DateInscription { get; set; }
        public DateTime? DateDesinscription { get; set; }

        public virtual ICollection<EtudiantNiveau> EtudiantNiveaus { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Presence> Presences { get; set; }
    }
}
