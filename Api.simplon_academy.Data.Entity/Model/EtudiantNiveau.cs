﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class EtudiantNiveau
    {
        public int Id { get; set; }
        public int? IdNiveau { get; set; }
        public int? IdEtudiant { get; set; }
        public DateOnly? DateDebut { get; set; }
        public DateOnly? DateFin { get; set; }

        public virtual Etudiant? IdEtudiantNavigation { get; set; }
        public virtual Niveau? IdNiveauNavigation { get; set; }
    }
}
