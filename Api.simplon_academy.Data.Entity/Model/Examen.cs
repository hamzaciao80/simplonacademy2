﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Examen
    {
        public Examen()
        {
            Notes = new HashSet<Note>();
        }

        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public int? IdCours { get; set; }
        public string? Sujet { get; set; }
        public string? Nombre { get; set; }

        public virtual Cour? IdCoursNavigation { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}
