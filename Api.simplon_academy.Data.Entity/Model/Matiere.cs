﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Matiere
    {
        public Matiere()
        {
            Cours = new HashSet<Cour>();
        }

        public int Id { get; set; }
        public string? Titre { get; set; }
        public string? Description { get; set; }
        public int? IdCategorie { get; set; }

        public virtual Categorie? IdCategorieNavigation { get; set; }
        public virtual ICollection<Cour> Cours { get; set; }
    }
}
