﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Niveau
    {
        public Niveau()
        {
            EtudiantNiveaus = new HashSet<EtudiantNiveau>();
        }

        public int Id { get; set; }
        public int? Année { get; set; }

        public virtual ICollection<EtudiantNiveau> EtudiantNiveaus { get; set; }
    }
}
