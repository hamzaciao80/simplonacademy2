﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Note
    {
        public int Id { get; set; }
        public int? IdExam { get; set; }
        public int? IdEtudiant { get; set; }
        public float? Note1 { get; set; }

        public virtual Etudiant? IdEtudiantNavigation { get; set; }
        public virtual Examen? IdExamNavigation { get; set; }
    }
}
