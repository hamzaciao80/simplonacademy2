﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Presence
    {
        public Presence()
        {
            Salles = new HashSet<Salle>();
        }

        public int Id { get; set; }
        public int? IdCours { get; set; }
        public DateTime? DateDebut { get; set; }
        public DateTime? DateFin { get; set; }
        public bool? EstPresent { get; set; }
        public int? IdEtudiant { get; set; }

        public virtual Cour? IdCoursNavigation { get; set; }
        public virtual Etudiant? IdEtudiantNavigation { get; set; }
        public virtual ICollection<Salle> Salles { get; set; }
    }
}
