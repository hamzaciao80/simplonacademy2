﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Professeur
    {
        public Professeur()
        {
            CoursProfesseurs = new HashSet<CoursProfesseur>();
            Salles = new HashSet<Salle>();
        }

        public int Id { get; set; }
        public string? Nom { get; set; }
        public string? Prenom { get; set; }
        public string? Email { get; set; }

        public virtual ICollection<CoursProfesseur> CoursProfesseurs { get; set; }
        public virtual ICollection<Salle> Salles { get; set; }
    }
}
