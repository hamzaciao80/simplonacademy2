﻿using System;
using System.Collections.Generic;

namespace Api.simplon_academy.Datas.Entities.Model
{
    public partial class Salle
    {
        public int Id { get; set; }
        public int IdCours { get; set; }
        public int IdProfesseur { get; set; }
        public int Idpresence { get; set; }
        public string? Name { get; set; }

        public virtual Cour IdCoursNavigation { get; set; } = null!;
        public virtual Professeur IdProfesseurNavigation { get; set; } = null!;
        public virtual Presence IdpresenceNavigation { get; set; } = null!;
    }
}
