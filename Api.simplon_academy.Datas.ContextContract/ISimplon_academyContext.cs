﻿using Api.simplon_academy.Datas.Entities.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Context.Contract
{
    public interface ISimplon_academyContext : IDbContext
    {

        DbSet<Categorie> Categories { get; set; }

        DbSet<Cour> Cours { get; set; }

        DbSet<CoursEtudiant> CoursEtudiants { get; set; }

        DbSet<CoursProfesseur> CoursProfesseurs { get; set; }

        DbSet<Cycle> Cycles { get; set; }

        DbSet<Etudiant> Etudiants { get; set; }

        DbSet<EtudiantNiveau> EtudiantNiveaus { get; set; }

        DbSet<Examen> Examens { get; set; }

        DbSet<Matiere> Matieres { get; set; }

        DbSet<Niveau> Niveaus { get; set; }

        DbSet<Note> Notes { get; set; }

        DbSet<Presence> Presences { get; set; }

        DbSet<Professeur> Professeurs { get; set; }

        DbSet<Salle> Salles { get; set; }
    }
}
