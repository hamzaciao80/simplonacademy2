﻿using Api.simplon_academy.Datas.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository.Contract
{
    public interface IRepositoryEtudiant : IGenericRepository<Etudiant>
    {
        Task<Etudiant> GetEtudiantByNameAsync(string name);

        Task<Etudiant> GetEtudiantByEmailAsync(string email);
    }
}
