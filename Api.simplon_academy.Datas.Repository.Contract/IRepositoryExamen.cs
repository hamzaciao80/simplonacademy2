﻿using Api.simplon_academy.Datas.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository.Contract
{
    public interface IRepositoryExamen : IGenericRepository<Examen>
    {
        Task<Examen> GetExamenByCoursID(int idCours);

    }
}
