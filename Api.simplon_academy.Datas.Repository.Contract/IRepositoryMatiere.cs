﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.simplon_academy.Datas.Entities.Model;

namespace Api.simplon_academy.Datas.Repository.Contract
{
    public interface IRepositoryMatiere : IGenericRepository<Matiere>
    {
        Task<Matiere> GetCycleById(int ID);

        Task<Matiere> GetCycleByName(string Name);
    }
}
