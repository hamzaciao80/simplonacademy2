﻿using Api.simplon_academy.Datas.Entities.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository.Contract
{
    public interface IRepositoryPresence : IGenericRepository<Presence>

    {   /// <summary>
        /// Permet de récupérer la présence d'un étudiant par son Id
        /// </summary>
        /// <param name="PresenceEtudiantId"></param>
        /// <returns></returns>
        Task<Presence> GetPresenceByIdEtudiantAsync(int PresenceEtudiantId);

    }
}
