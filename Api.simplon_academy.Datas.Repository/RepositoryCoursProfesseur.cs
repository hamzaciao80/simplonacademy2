﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryCoursProfesseur : GenericRepository<CoursProfesseur>, IRepositoryCoursProfesseur
    {
        public RepositoryCoursProfesseur(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }
    }

}
