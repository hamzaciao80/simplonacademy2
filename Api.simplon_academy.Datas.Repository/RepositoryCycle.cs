﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryCycle : GenericRepository<Cycle>, IRepositoryCycle
    {
        public RepositoryCycle(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Cycle> GetCycleById(int Id)
        {
            return await _simplon_academyContext.Cycles.FirstOrDefaultAsync(e => e.Id == Id).ConfigureAwait(false);
        }



    public async Task<Cycle> GetCycleByNameAsync(string nom)
        {
            return await _simplon_academyContext.Cycles.FirstOrDefaultAsync(e => e.Titre == nom).ConfigureAwait(false);
        }
    }

}
