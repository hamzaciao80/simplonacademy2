﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryEtudiant : GenericRepository<Etudiant>, IRepositoryEtudiant
    {
        public RepositoryEtudiant(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Etudiant> GetEtudiantByEmailAsync(string email)
        {
            return await _simplon_academyContext.Etudiants.FirstOrDefaultAsync(e => e.Email == email).ConfigureAwait(false);
        }

        public async Task<Etudiant> GetEtudiantByNameAsync(string nom)
        {
            return await _simplon_academyContext.Etudiants.FirstOrDefaultAsync(e => e.Nom == nom).ConfigureAwait(false);
        }
    }
}
