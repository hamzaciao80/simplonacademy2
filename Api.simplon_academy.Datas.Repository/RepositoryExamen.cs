﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryExamen : GenericRepository<Examen>, IRepositoryExamen
    {
        public RepositoryExamen(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Examen> GetExamenByCoursID(int idCours)
        {
            return await _simplon_academyContext.Examens.FirstOrDefaultAsync(x => x.IdCours == idCours).ConfigureAwait(false);
        }
    }
}
