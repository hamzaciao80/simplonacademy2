﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    internal class RepositoryMatiere : GenericRepository<Matiere>, IRepositoryMatiere
    {
        public RepositoryMatiere(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Matiere> GetCycleById(int ID)
        {
            return await _simplon_academyContext.Matieres.FirstOrDefaultAsync(e => e.Id == ID).ConfigureAwait(false);
        }

        public async Task<Matiere> GetCycleByName(string Name)
        {
            return await _simplon_academyContext.Matieres.FirstOrDefaultAsync(e => e.Titre == Name).ConfigureAwait(false);
        }
    }
}
