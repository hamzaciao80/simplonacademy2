﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    internal class RepositoryNote : GenericRepository<Note>, IRepositoryNote
    {
        public RepositoryNote(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Note> GetNoteByIdAsync(int Id)
        {
            return await _simplon_academyContext.Notes.FirstOrDefaultAsync(e => e.Id == Id).ConfigureAwait(false);
        }

        public async Task<Note> GetNoteByExamStudentAsync(int idexam, int idStudent)
        {
            return await _simplon_academyContext.Notes.FirstOrDefaultAsync(e => e.IdExam == idexam && e.IdEtudiant == idStudent).ConfigureAwait(false);
        }
    }
}
