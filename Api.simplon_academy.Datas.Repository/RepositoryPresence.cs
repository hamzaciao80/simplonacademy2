﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryPresence : GenericRepository<Presence>, IRepositoryPresence
    {   /// <summary>
        /// Initialise une nouvelle instance
        /// </summary>
        /// <param name="simplon_academyContext"></param>
        public RepositoryPresence(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Presence> GetPresenceByIdEtudiantAsync(int PresenceEtudiantId)
        {
            return await _simplon_academyContext.Presences.FirstOrDefaultAsync(x => x.IdEtudiant == PresenceEtudiantId).ConfigureAwait(false);

        }


    }
}
