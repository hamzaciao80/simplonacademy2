﻿using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Api.simplon_academy.Datas.Repository
{
    public class RepositoryProfesseur : GenericRepository<Professeur>, IRepositoryProfesseur
    {   /// <summary>
    /// Initialise une nouvelle instance
    /// </summary>
    /// <param name="simplon_academyContext"></param>
        public RepositoryProfesseur(ISimplon_academyContext simplon_academyContext) : base(simplon_academyContext)
        {
        }

        public async Task<Professeur> GetProfesseurByMailAsync(string ProfesseurMail)
        {
            return await _simplon_academyContext.Professeurs.FirstOrDefaultAsync(x => x.Email == ProfesseurMail).ConfigureAwait(false);

        }
    }
}