﻿using Api.SA.Business.Service;
using Api.SA.Business.Srvc.Ctr;
using Api.simplon_academy.Datas.Context.Contract;
using Api.simplon_academy.Datas.Entities;
using Api.simplon_academy.Datas.Repository;
using Api.simplon_academy.Datas.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Api.simplon_academy.IoC.WebApi
{
    public static class IoCApplication
    {
        public static IServiceCollection ConfigureInjectionDependencyRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryEtudiant, RepositoryEtudiant>();
            services.AddScoped<IRepositoryExamen, RepositoryExamen>();
            services.AddScoped<IRepositoryNiveau, RepositoryNiveau>();
            services.AddScoped<IRepositoryCoursProfesseur, RepositoryCoursProfesseur>();
            services.AddScoped<IRepositoryPresence, RepositoryPresence>();
            services.AddScoped<IRepositoryProfesseur, RepositoryProfesseur>();

            return services;
        }

        public static IServiceCollection ConfigureDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("BddConnection");

            services.AddDbContext<ISimplon_academyContext, SimplonUnivDBContext>(options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors());

            return services;
        }

        public static IServiceCollection ConfigureInjectionDependencyService(this IServiceCollection services)
        {
            // Injections des Dépendances
            // - Service

            services.AddScoped<IServiceEtudiant, ServiceEtudiant>();

            return services;
        }
    }
}