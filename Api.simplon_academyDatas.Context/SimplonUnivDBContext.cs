﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Api.simplon_academy.Datas.Entities.Model;
using Api.simplon_academy.Datas.Context.Contract;

namespace Api.simplon_academy.Datas.Entities
{
    public class SimplonUnivDBContext : DbContext, ISimplon_academyContext
    {
        public SimplonUnivDBContext(DbContextOptions<SimplonUnivDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Categorie> Categories { get; set; } = null!;
        public virtual DbSet<Cour> Cours { get; set; } = null!;
        public virtual DbSet<CoursEtudiant> CoursEtudiants { get; set; } = null!;
        public virtual DbSet<CoursProfesseur> CoursProfesseurs { get; set; } = null!;
        public virtual DbSet<Cycle> Cycles { get; set; } = null!;
        public virtual DbSet<Etudiant> Etudiants { get; set; } = null!;
        public virtual DbSet<EtudiantNiveau> EtudiantNiveaus { get; set; } = null!;
        public virtual DbSet<Examen> Examens { get; set; } = null!;
        public virtual DbSet<Matiere> Matieres { get; set; } = null!;
        public virtual DbSet<Niveau> Niveaus { get; set; } = null!;
        public virtual DbSet<Note> Notes { get; set; } = null!;
        public virtual DbSet<Presence> Presences { get; set; } = null!;
        public virtual DbSet<Professeur> Professeurs { get; set; } = null!;
        public virtual DbSet<Salle> Salles { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");

            modelBuilder.Entity<Categorie>(entity =>
            {
                entity.ToTable("categorie");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Label).HasMaxLength(255);
            });

            modelBuilder.Entity<Cour>(entity =>
            {
                entity.ToTable("cours");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdCycle, "fk_cours_cycle");

                entity.HasIndex(e => e.IdMatiere, "fk_cours_matiere");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.EstOuvert).HasDefaultValueSql("'0'");

                entity.Property(e => e.IdCycle).HasColumnType("int(11)");

                entity.Property(e => e.IdMatiere).HasColumnType("int(11)");

                entity.HasOne(d => d.IdCycleNavigation)
                    .WithMany(p => p.Cours)
                    .HasForeignKey(d => d.IdCycle)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cours_cycle");

                entity.HasOne(d => d.IdMatiereNavigation)
                    .WithMany(p => p.Cours)
                    .HasForeignKey(d => d.IdMatiere)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cours_matiere");
            });

            modelBuilder.Entity<CoursEtudiant>(entity =>
            {
                entity.ToTable("cours_etudiant");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdCours, "fk_cours_cours");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdCours).HasColumnType("int(11)");

                entity.Property(e => e.IdEtudiant).HasColumnType("int(11)");

                entity.HasOne(d => d.IdCoursNavigation)
                    .WithMany(p => p.CoursEtudiants)
                    .HasForeignKey(d => d.IdCours)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cours_cours");
            });

            modelBuilder.Entity<CoursProfesseur>(entity =>
            {
                entity.ToTable("cours_professeurs");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdCours, "Fk_cours");

                entity.HasIndex(e => e.IdProfesseur, "Fk_professeurs");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdCours).HasColumnType("int(11)");

                entity.Property(e => e.IdProfesseur).HasColumnType("int(11)");

                entity.HasOne(d => d.IdCoursNavigation)
                    .WithMany(p => p.CoursProfesseurs)
                    .HasForeignKey(d => d.IdCours)
                    .HasConstraintName("Fk_cours");

                entity.HasOne(d => d.IdProfesseurNavigation)
                    .WithMany(p => p.CoursProfesseurs)
                    .HasForeignKey(d => d.IdProfesseur)
                    .HasConstraintName("Fk_professeurs");
            });

            modelBuilder.Entity<Cycle>(entity =>
            {
                entity.ToTable("cycles");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateDebut).HasColumnType("datetime");

                entity.Property(e => e.DateFin).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Titre).HasMaxLength(200);
            });

            modelBuilder.Entity<Etudiant>(entity =>
            {
                entity.ToTable("etudiant");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Adresse).HasMaxLength(255);

                entity.Property(e => e.DateAnniversaire).HasColumnType("datetime");

                entity.Property(e => e.DateDesinscription)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'0000-00-00 00:00:00'");

                entity.Property(e => e.DateInscription).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.Nom).HasMaxLength(255);

                entity.Property(e => e.Prenom).HasMaxLength(255);

                entity.Property(e => e.Telephone).HasMaxLength(255);
            });

            modelBuilder.Entity<EtudiantNiveau>(entity =>
            {
                entity.ToTable("etudiant_niveau");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdEtudiant, "Fk_Etudiant");

                entity.HasIndex(e => e.IdNiveau, "Fk_Niveau");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdEtudiant).HasColumnType("int(11)");

                entity.Property(e => e.IdNiveau).HasColumnType("int(11)");

                entity.HasOne(d => d.IdEtudiantNavigation)
                    .WithMany(p => p.EtudiantNiveaus)
                    .HasForeignKey(d => d.IdEtudiant)
                    .HasConstraintName("Fk_Etudiant");

                entity.HasOne(d => d.IdNiveauNavigation)
                    .WithMany(p => p.EtudiantNiveaus)
                    .HasForeignKey(d => d.IdNiveau)
                    .HasConstraintName("Fk_Niveau");
            });

            modelBuilder.Entity<Examen>(entity =>
            {
                entity.ToTable("examens");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdCours, "examens_cours");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.IdCours).HasColumnType("int(11)");

                entity.Property(e => e.Nombre).HasMaxLength(250);

                entity.Property(e => e.Sujet).HasMaxLength(250);

                entity.HasOne(d => d.IdCoursNavigation)
                    .WithMany(p => p.Examen)
                    .HasForeignKey(d => d.IdCours)
                    .HasConstraintName("examens_cours");
            });

            modelBuilder.Entity<Matiere>(entity =>
            {
                entity.ToTable("matiere");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdCategorie, "Fk_matiere");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.IdCategorie).HasColumnType("int(11)");

                entity.Property(e => e.Titre).HasMaxLength(255);

                entity.HasOne(d => d.IdCategorieNavigation)
                    .WithMany(p => p.Matieres)
                    .HasForeignKey(d => d.IdCategorie)
                    .HasConstraintName("Fk_matiere");
            });

            modelBuilder.Entity<Niveau>(entity =>
            {
                entity.ToTable("niveau");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Année).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.ToTable("note");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdEtudiant, " Fk_etudient");

                entity.HasIndex(e => e.IdExam, "Fk_Examen");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdEtudiant).HasColumnType("int(11)");

                entity.Property(e => e.IdExam).HasColumnType("int(11)");

                entity.Property(e => e.Note1).HasColumnName("Note");

                entity.HasOne(d => d.IdEtudiantNavigation)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.IdEtudiant)
                    .HasConstraintName(" Fk_etudient");

                entity.HasOne(d => d.IdExamNavigation)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.IdExam)
                    .HasConstraintName("Fk_Examen");
            });

            modelBuilder.Entity<Presence>(entity =>
            {
                entity.ToTable("presence");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.IdEtudiant, "Fk_presence");

                entity.HasIndex(e => e.IdCours, "Fk_presence_cours");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateDebut).HasColumnType("datetime");

                entity.Property(e => e.DateFin).HasColumnType("datetime");

                entity.Property(e => e.IdCours).HasColumnType("int(11)");

                entity.Property(e => e.IdEtudiant).HasColumnType("int(11)");

                entity.HasOne(d => d.IdCoursNavigation)
                    .WithMany(p => p.Presences)
                    .HasForeignKey(d => d.IdCours)
                    .HasConstraintName("Fk_presence_cours");

                entity.HasOne(d => d.IdEtudiantNavigation)
                    .WithMany(p => p.Presences)
                    .HasForeignKey(d => d.IdEtudiant)
                    .HasConstraintName("Fk_presence");
            });

            modelBuilder.Entity<Professeur>(entity =>
            {
                entity.ToTable("professeurs");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.Nom).HasMaxLength(255);

                entity.Property(e => e.Prenom).HasMaxLength(255);
            });

            modelBuilder.Entity<Salle>(entity =>
            {
                entity.ToTable("salle");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.Idpresence, "fk_salle_cours");

                entity.HasIndex(e => e.IdCours, "fk_salle_presence");

                entity.HasIndex(e => e.IdProfesseur, "fk_salle_prof");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.IdCours).HasColumnType("int(11)");

                entity.Property(e => e.IdProfesseur).HasColumnType("int(11)");

                entity.Property(e => e.Idpresence).HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'null'");

                entity.HasOne(d => d.IdCoursNavigation)
                    .WithMany(p => p.Salles)
                    .HasForeignKey(d => d.IdCours)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_salle_presence");

                entity.HasOne(d => d.IdProfesseurNavigation)
                    .WithMany(p => p.Salles)
                    .HasForeignKey(d => d.IdProfesseur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_salle_prof");

                entity.HasOne(d => d.IdpresenceNavigation)
                    .WithMany(p => p.Salles)
                    .HasForeignKey(d => d.Idpresence)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_salle_cours");
            });

            //OnModelCreating(modelBuilder);
        }
        //protected override void OnModelCreatingPartial(ModelBuilder modelBuilder)
        //{

        //}
    }
}
