-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 100.68.51.113:32768
-- Généré le : lun. 27 fév. 2023 à 16:11
-- Version du serveur : 10.10.2-MariaDB-1:10.10.2+maria~ubu2204
-- Version de PHP : 8.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simplonAcademy`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `Id` int(11) NOT NULL,
  `Label` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`Id`, `Label`) VALUES
(1, 'Informatique'),
(2, 'Arts'),
(3, 'Philosophie'),
(4, 'Sciences sociales'),
(5, 'Droit'),
(6, 'Psychologie'),
(7, 'Langues étrangères ');

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `Id` int(11) NOT NULL,
  `IdMatiere` int(11) NOT NULL,
  `IdCycle` int(11) NOT NULL,
  `DateDebut` date DEFAULT NULL,
  `DateFin` date DEFAULT NULL,
  `EstOuvert` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`Id`, `IdMatiere`, `IdCycle`, `DateDebut`, `DateFin`, `EstOuvert`) VALUES
(3, 1, 3, '2023-02-01', '2023-02-02', 1),
(4, 2, 2, '2023-02-03', '2023-02-04', 1),
(5, 7, 2, '2023-02-05', '2023-02-06', 0),
(9, 4, 2, '2023-02-07', '2023-02-08', 0),
(10, 8, 1, '2023-02-08', '2023-02-09', 0);

-- --------------------------------------------------------

--
-- Structure de la table `cours_etudiant`
--

CREATE TABLE `cours_etudiant` (
  `Id` int(11) NOT NULL,
  `IdCours` int(11) NOT NULL,
  `IdEtudiant` int(11) NOT NULL,
  `DateDebut` date DEFAULT NULL,
  `DateFin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cours_etudiant`
--

INSERT INTO `cours_etudiant` (`Id`, `IdCours`, `IdEtudiant`, `DateDebut`, `DateFin`) VALUES
(1, 4, 3, '2023-02-01', '2023-02-02'),
(2, 3, 3, '2023-02-03', '2023-02-04'),
(5, 3, 2, '2023-02-03', '2023-02-04'),
(6, 3, 1, '1900-01-08', '1900-01-09');

-- --------------------------------------------------------

--
-- Structure de la table `cours_professeurs`
--

CREATE TABLE `cours_professeurs` (
  `Id` int(11) NOT NULL,
  `IdCours` int(11) DEFAULT NULL,
  `IdProfesseur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cours_professeurs`
--

INSERT INTO `cours_professeurs` (`Id`, `IdCours`, `IdProfesseur`) VALUES
(1, 3, 1),
(2, 10, 2),
(3, 5, 4),
(4, 9, 5);

-- --------------------------------------------------------

--
-- Structure de la table `cycles`
--

CREATE TABLE `cycles` (
  `Id` int(11) NOT NULL,
  `Titre` varchar(200) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `DateDebut` datetime DEFAULT NULL,
  `DateFin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `cycles`
--

INSERT INTO `cycles` (`Id`, `Titre`, `Description`, `DateDebut`, `DateFin`) VALUES
(1, 'Cycle préparatoire', 'il s\'agit d\'un cycle de deux années d\'études supérieures destiné aux étudiants qui souhaitent intégrer une grande école ou une filière sélective.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Programme Bachelor', 'l s\'agit d\'un cycle de trois années d\'études qui permet d\'obtenir le diplôme de Bachelor. Cette formation est générale et pluridisciplinaire', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Programme Grande École', 'il s\'agit d\'un cycle de deux à cinq années d\'études qui permet d\'obtenir le diplôme d\'ingénieur, de manager, ou autre diplôme de niveau Bac+5.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Mastère spécialisé', 'il s\'agit d\'un cycle d\'un an d\'études qui permet d\'obtenir le diplôme de Mastère spécialisé.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `Id` int(11) NOT NULL,
  `Nom` varchar(255) DEFAULT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Adresse` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Telephone` varchar(255) DEFAULT NULL,
  `DateAnniversaire` datetime DEFAULT NULL,
  `DateInscription` datetime DEFAULT NULL,
  `DateDesinscription` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`Id`, `Nom`, `Prenom`, `Adresse`, `Email`, `Telephone`, `DateAnniversaire`, `DateInscription`, `DateDesinscription`) VALUES
(1, 'hamza', 'bely', '2 rue hamilton', 'hamzaciao80@gmail.com', '070700707', '2005-09-07 15:23:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'ali', 'bely', '2 rue hamilton', 'hamzaciao80@gmail.com', '070700707', '2003-02-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'matilde', 'armani', '2 rue lyopn24', 'armani80@gmail.com', '070700707', '1909-12-09 15:25:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'karim', 'gucci', '2 rue lyopn24', 'gucci80@gmail.com', '070700707', '1909-12-03 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'giorgio', 'volani', '2 rue lyopn24', 'volani@gmail.com', '070700707', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant_niveau`
--

CREATE TABLE `etudiant_niveau` (
  `Id` int(11) NOT NULL,
  `IdNiveau` int(11) DEFAULT NULL,
  `IdEtudiant` int(11) DEFAULT NULL,
  `DateDebut` date DEFAULT NULL,
  `DateFin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `etudiant_niveau`
--

INSERT INTO `etudiant_niveau` (`Id`, `IdNiveau`, `IdEtudiant`, `DateDebut`, `DateFin`) VALUES
(1, 1, 2, '0000-00-00', '0000-00-00'),
(2, 2, 4, '2019-02-20', '2021-06-12'),
(3, 3, 5, '2021-01-05', '2023-02-15');

-- --------------------------------------------------------

--
-- Structure de la table `examens`
--

CREATE TABLE `examens` (
  `Id` int(11) NOT NULL,
  `Date` datetime DEFAULT NULL,
  `IdCours` int(11) DEFAULT NULL,
  `Sujet` varchar(250) DEFAULT NULL,
  `Nombre` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `Id` int(11) NOT NULL,
  `Titre` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `IdCategorie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`Id`, `Titre`, `Description`, `IdCategorie`) VALUES
(1, 'Programmation', 'description programmation', 1),
(2, 'Danse', 'description danse', 2),
(3, 'Éthique', 'description éthique', 3),
(4, 'Sociologie', 'description sociologie', 4),
(6, 'Droit pénal', 'description droit pénal', 5),
(7, 'Neuropsychologie', 'description neuropsychologie', 6),
(8, 'Anglais', 'description anglais', 7);

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `Id` int(11) NOT NULL,
  `Année` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`Id`, `Année`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `Id` int(11) NOT NULL,
  `IdExam` int(11) DEFAULT NULL,
  `IdEtudiant` int(11) DEFAULT NULL,
  `Note` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `presence`
--

CREATE TABLE `presence` (
  `Id` int(11) NOT NULL,
  `IdCours` int(11) DEFAULT NULL,
  `DateDebut` datetime DEFAULT NULL,
  `DateFin` datetime DEFAULT NULL,
  `EstPresent` tinyint(1) DEFAULT NULL,
  `IdEtudiant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `presence`
--

INSERT INTO `presence` (`Id`, `IdCours`, `DateDebut`, `DateFin`, `EstPresent`, `IdEtudiant`) VALUES
(1, 4, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 1),
(2, 4, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 2),
(3, 4, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 3),
(4, 5, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 4),
(5, 9, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 5),
(6, 9, '2000-04-12 00:00:00', '2000-04-12 00:00:00', 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `professeurs`
--

CREATE TABLE `professeurs` (
  `Id` int(11) NOT NULL,
  `Nom` varchar(255) DEFAULT NULL,
  `Prenom` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `professeurs`
--

INSERT INTO `professeurs` (`Id`, `Nom`, `Prenom`, `Email`) VALUES
(1, 'Kake', 'Abdou', 'abdou.kake@gmail.com'),
(2, 'Lahrica', 'Fany', 'fany.lahrica@gmail.com'),
(3, 'Dupont', 'Joy', 'joy.dupont@gmail.com'),
(4, 'Reguep', 'Tom', 'tom.reguep@gmail.com'),
(5, 'Roche', 'Lisa', 'lisa.roche@gmail.com'),
(6, 'Van', 'Angele', 'angele.van@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `Id` int(11) NOT NULL,
  `IdCours` int(11) NOT NULL,
  `IdProfesseur` int(11) NOT NULL,
  `Idpresence` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT 'null'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`Id`, `IdCours`, `IdProfesseur`, `Idpresence`, `Name`) VALUES
(1, 5, 1, 1, 'CSS'),
(2, 4, 3, 2, 'JAVA SCRIPT'),
(3, 3, 4, 4, 'PYTON');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_cours_cycle` (`IdCycle`),
  ADD KEY `fk_cours_matiere` (`IdMatiere`);

--
-- Index pour la table `cours_etudiant`
--
ALTER TABLE `cours_etudiant`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `cours_professeurs`
--
ALTER TABLE `cours_professeurs`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Fk_professeurs` (`IdProfesseur`),
  ADD KEY `Fk_cours` (`IdCours`);

--
-- Index pour la table `cycles`
--
ALTER TABLE `cycles`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `etudiant_niveau`
--
ALTER TABLE `etudiant_niveau`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Fk_Etudiant` (`IdEtudiant`),
  ADD KEY `Fk_Niveau` (`IdNiveau`);

--
-- Index pour la table `examens`
--
ALTER TABLE `examens`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `examens_cours` (`IdCours`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Fk_matiere` (`IdCategorie`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Fk_Examen` (`IdExam`),
  ADD KEY ` Fk_etudient` (`IdEtudiant`);

--
-- Index pour la table `presence`
--
ALTER TABLE `presence`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Fk_presence` (`IdEtudiant`),
  ADD KEY `Fk_presence_cours` (`IdCours`);

--
-- Index pour la table `professeurs`
--
ALTER TABLE `professeurs`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_salle_prof` (`IdProfesseur`),
  ADD KEY `fk_salle_presence` (`IdCours`),
  ADD KEY `fk_salle_cours` (`Idpresence`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `cours`
--
ALTER TABLE `cours`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `cours_etudiant`
--
ALTER TABLE `cours_etudiant`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `cours_professeurs`
--
ALTER TABLE `cours_professeurs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `cycles`
--
ALTER TABLE `cycles`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `etudiant_niveau`
--
ALTER TABLE `etudiant_niveau`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `examens`
--
ALTER TABLE `examens`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `note`
--
ALTER TABLE `note`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `presence`
--
ALTER TABLE `presence`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `professeurs`
--
ALTER TABLE `professeurs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `fk_cours_cycle` FOREIGN KEY (`IdCycle`) REFERENCES `cycles` (`Id`),
  ADD CONSTRAINT `fk_cours_matiere` FOREIGN KEY (`IdMatiere`) REFERENCES `matiere` (`Id`);

--
-- Contraintes pour la table `cours_etudiant`
--
ALTER TABLE `cours_etudiant`
  ADD CONSTRAINT `fk_cours_cours` FOREIGN KEY (`IdCours`) REFERENCES `cours` (`Id`);

--
-- Contraintes pour la table `cours_professeurs`
--
ALTER TABLE `cours_professeurs`
  ADD CONSTRAINT `Fk_cours` FOREIGN KEY (`IdCours`) REFERENCES `cours` (`Id`),
  ADD CONSTRAINT `Fk_professeurs` FOREIGN KEY (`IdProfesseur`) REFERENCES `professeurs` (`Id`);

--
-- Contraintes pour la table `etudiant_niveau`
--
ALTER TABLE `etudiant_niveau`
  ADD CONSTRAINT `Fk_Etudiant` FOREIGN KEY (`IdEtudiant`) REFERENCES `etudiant` (`Id`),
  ADD CONSTRAINT `Fk_Niveau` FOREIGN KEY (`IdNiveau`) REFERENCES `niveau` (`Id`);

--
-- Contraintes pour la table `examens`
--
ALTER TABLE `examens`
  ADD CONSTRAINT `examens_cours` FOREIGN KEY (`IdCours`) REFERENCES `cours` (`Id`);

--
-- Contraintes pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `Fk_matiere` FOREIGN KEY (`IdCategorie`) REFERENCES `categorie` (`Id`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT ` Fk_etudient` FOREIGN KEY (`IdEtudiant`) REFERENCES `etudiant` (`Id`),
  ADD CONSTRAINT `Fk_Examen` FOREIGN KEY (`IdExam`) REFERENCES `examens` (`Id`);

--
-- Contraintes pour la table `presence`
--
ALTER TABLE `presence`
  ADD CONSTRAINT `Fk_presence` FOREIGN KEY (`IdEtudiant`) REFERENCES `etudiant` (`Id`),
  ADD CONSTRAINT `Fk_presence_cours` FOREIGN KEY (`IdCours`) REFERENCES `cours` (`Id`);

--
-- Contraintes pour la table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `fk_salle_cours` FOREIGN KEY (`Idpresence`) REFERENCES `presence` (`Id`),
  ADD CONSTRAINT `fk_salle_presence` FOREIGN KEY (`IdCours`) REFERENCES `cours` (`Id`),
  ADD CONSTRAINT `fk_salle_prof` FOREIGN KEY (`IdProfesseur`) REFERENCES `professeurs` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
