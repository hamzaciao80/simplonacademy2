﻿using Api.SA.Business.Model.Etudiants;
using Api.SA.Business.Srvc.Ctr;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace simplon_academy.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EtudiantsController : ControllerBase
    {

        private readonly IServiceEtudiant _serviceEtudiant;

        public EtudiantsController(IServiceEtudiant serviceEtudiant)
        {
            _serviceEtudiant = serviceEtudiant;
        }

        /*
         * Get: api/Etudiants
         * get all students
        */
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<EtudiantReadDTO>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> EtudiantListAsync()
        {
            var etudiants = await _serviceEtudiant.GetListEtudiantAsync().ConfigureAwait(false);

            return Ok(etudiants);
        }

        /*
         * Get: api/EtudiantByMail
         * get student by his mail
        */
        [HttpGet("{mailEtudiant}")]
        [ProducesResponseType(typeof(IEnumerable<EtudiantReadDTO>), 200)]
        [ProducesResponseType(typeof(StatusCodeResult), 500)]
        [ProducesResponseType(typeof(StatusCodeResult), 400)]
        public async Task<ActionResult> EtudiantByMailAsync(string mailEtudiant)
        {
            var etudiants = await _serviceEtudiant.GetListEtudiantAsync().ConfigureAwait(false);

            return Ok(etudiants);
        }

        /**
            // GET: api/<ValuesController>
            [HttpGet]
            public IEnumerable<string> Get()
            {
                return new string[] { "value1", "value2" };
            }

            // GET api/<ValuesController>/5
            [HttpGet("{id}")]
            public string Get(int id)
            {
                return "value";
            }

            // POST api/<ValuesController>
            [HttpPost]
            public void Post([FromBody] string value)
            {
            }

            // PUT api/<ValuesController>/5
            [HttpPut("{id}")]
            public void Put(int id, [FromBody] string value)
            {
            }

            // DELETE api/<ValuesController>/5
            [HttpDelete("{id}")]
            public void Delete(int id)
            {
            }
        **/
    }
}
